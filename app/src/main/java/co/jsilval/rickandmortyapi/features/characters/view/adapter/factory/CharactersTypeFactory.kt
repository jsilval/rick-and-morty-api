package co.jsilval.rickandmortyapi.features.characters.view.adapter.factory

import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import co.jsilval.rickandmortyapi.features.characters.view.adapter.entities.Character
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewholder.AbstractViewHolder

interface CharactersTypeFactory {
    fun type(character: Character): Int
    fun createViewHolder(binding: ViewBinding, type: Int): AbstractViewHolder<*>
    fun createViewBinding(parent: ViewGroup, type: Int): ViewBinding
}
