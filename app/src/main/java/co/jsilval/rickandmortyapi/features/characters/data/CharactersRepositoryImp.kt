package co.jsilval.rickandmortyapi.features.characters.data

import android.net.Uri
import co.jsilval.rickandmortyapi.core.Result
import co.jsilval.rickandmortyapi.core.common.EMPTY_RESULT
import co.jsilval.rickandmortyapi.core.common.ErrorType
import co.jsilval.rickandmortyapi.core.rest.DataBoundResource
import co.jsilval.rickandmortyapi.features.characters.data.local.CharactersLocalDataSource
import co.jsilval.rickandmortyapi.features.characters.data.remote.CharactersRemoteDataSource
import co.jsilval.rickandmortyapi.features.characters.data.remote.entities.ApiCharacterResponse
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewmodels.AbstractCharacterViewModel
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewmodels.CharacterViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CharactersRepositoryImp @Inject constructor(
    private val remoteDataSource: CharactersRemoteDataSource,
    private val localDataSource: CharactersLocalDataSource,
) : CharactersRepository {

    var page: String? = null

    override suspend fun getCharacters(hasInternet: Boolean): Flow<Result<List<AbstractCharacterViewModel>, @ErrorType Int>> {
        return object :
            DataBoundResource<List<AbstractCharacterViewModel>, ApiCharacterResponse>() {
            override fun shouldFetch() = hasInternet

            override suspend fun saveCallServiceResult(response: ApiCharacterResponse) =
                withContext(Dispatchers.Default) {
                    page = if (response.info.next == null) {
                        null
                    } else {
                        val uri = Uri.parse(response.info.next)
                        uri.getQueryParameter("page")
                    }
                    localDataSource.saveToDB(response)
                }

            override suspend fun executeCallService(): ApiCharacterResponse? {
                return remoteDataSource.getCharacters(page)
            }

            override fun isNotEmptyResult(result: ApiCharacterResponse): Boolean {
                return result.results?.isNotEmpty() ?: false
            }

            override fun loadFromDb(): Flow<Result<List<AbstractCharacterViewModel>, @ErrorType Int>> {
                return localDataSource.getCharacters().map { characters ->
                    if (characters.isEmpty()) {
                        Result.Error(EMPTY_RESULT)
                    } else {
                        Result.Success(
                            characters.map {
                                CharacterViewModel(it.toCharacterModel())
                            }
                        )
                    }
                }
            }
        }.asFlow()
    }

    override suspend fun getCharacterDetail(id: Long) = flow {
        val model = localDataSource.getCharacterById(id).toCharacterDetailModel()
        emit(model)
    }.flowOn(Dispatchers.IO)
}