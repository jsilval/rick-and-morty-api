package co.jsilval.rickandmortyapi.features.characters.data.local

import co.jsilval.rickandmortyapi.core.db.character.CharacterDB
import co.jsilval.rickandmortyapi.core.db.character.CharacterDao
import co.jsilval.rickandmortyapi.features.characters.data.remote.entities.ApiCharacterResponse
import javax.inject.Inject

class CharactersLocalDataSource @Inject constructor(private val characterDao: CharacterDao) {

    fun getCharacters() = characterDao.getCharacters()

    fun saveToDB(response: ApiCharacterResponse) {
        val charactersDB = response.results?.map {
            CharacterDB(
                it.id,
                it.name,
                it.status,
                it.species,
                it.type,
                it.gender,
                it.image
            )
        }
        charactersDB?.let {
            characterDao.insert(charactersDB)
        }
    }

    fun getCharacterById(id: Long) = characterDao.getCharacterById(id)
}