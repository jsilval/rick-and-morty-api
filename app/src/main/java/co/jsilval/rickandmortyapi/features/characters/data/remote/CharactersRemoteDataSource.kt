package co.jsilval.rickandmortyapi.features.characters.data.remote

import android.content.Context
import co.jsilval.apimanager.core.rest.network.ApiClient
import co.jsilval.apimanager.core.rest.network.NetworkResponse
import co.jsilval.rickandmortyapi.features.characters.data.remote.entities.ApiCharacterResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class CharactersRemoteDataSource @Inject constructor(
    val context: Context,
    @Named("character") private val characterClient: ApiClient,
) {

    suspend fun getCharacters(page: String?): ApiCharacterResponse? = withContext(Dispatchers.IO) {
        page?.let { characterClient.serverRequest.field("page", it) }
        characterClient.execute(object : NetworkResponse<ApiCharacterResponse>() {})
    }
}