package co.jsilval.rickandmortyapi.features.main.di

import androidx.fragment.app.FragmentManager
import co.jsilval.rickandmortyapi.features.main.ui.MainActivity
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {
    companion object {
        @Provides
        fun provideFragmentManager(activity: MainActivity): FragmentManager {
            return activity.supportFragmentManager
        }
    }
}