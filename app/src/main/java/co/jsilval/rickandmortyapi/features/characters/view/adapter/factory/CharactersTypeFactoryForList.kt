package co.jsilval.rickandmortyapi.features.characters.view.adapter.factory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import co.jsilval.rickandmortyapi.databinding.CharacterItemLayoutBinding
import co.jsilval.rickandmortyapi.features.characters.view.adapter.entities.Character
import co.jsilval.rickandmortyapi.features.characters.view.adapter.exception.TypeNotSupportedException
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewholder.AbstractViewHolder
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewholder.CharacterViewHolder

class CharactersTypeFactoryForList : CharactersTypeFactory {

    override fun type(character: Character): Int = CharacterViewHolder.LAYOUT

    override fun createViewHolder(binding: ViewBinding, type: Int): AbstractViewHolder<*> {
        return when (type) {
            CharacterViewHolder.LAYOUT -> CharacterViewHolder(binding as CharacterItemLayoutBinding)
            else -> throw TypeNotSupportedException.create(String.format("LayoutType: %d", type))
        }
    }

    override fun createViewBinding(parent: ViewGroup, type: Int): ViewBinding {
        return when (type) {
            CharacterViewHolder.LAYOUT -> {
                val binding = CharacterItemLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                binding
            }
            else -> throw TypeNotSupportedException.create(String.format("ViewBindingType: %d", type))
        }
    }
}
