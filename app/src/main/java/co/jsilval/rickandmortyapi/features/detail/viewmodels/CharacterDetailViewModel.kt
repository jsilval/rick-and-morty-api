package co.jsilval.rickandmortyapi.features.detail.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.jsilval.rickandmortyapi.core.log.AppLog
import co.jsilval.rickandmortyapi.features.characters.domain.usecase.GetCharacters
import co.jsilval.rickandmortyapi.features.detail.domain.entities.CharacterDetail
import co.jsilval.rickandmortyapi.features.detail.viewmodels.CharacterDetailViewModel.CharacterDetailFragmentState.Init
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

class CharacterDetailViewModel @Inject constructor(private val getCharacters: GetCharacters) :
    ViewModel() {
    private val _state = MutableStateFlow<CharacterDetailFragmentState>(Init)
    val state: SharedFlow<CharacterDetailFragmentState> get() = _state

    private val _detail = MutableStateFlow<CharacterDetail?>(null)
    val detail: SharedFlow<CharacterDetail?> get() = _detail

    private fun showLoading() {
        _state.value = CharacterDetailFragmentState.IsLoading(true)
    }

    private fun hideLoading() {
        _state.value = CharacterDetailFragmentState.IsLoading(false)
    }

    fun getCharacterDetail(characterId: Long) {
        viewModelScope.launch {
            getCharacters.getDetailCharacter(characterId).onStart {
                showLoading()
            }.catch { exception ->
                AppLog.e("Error obteniendo detalle.", exception)
            }.collect {
                _detail.value = it
                hideLoading()
            }
        }
    }

    sealed class CharacterDetailFragmentState {
        object Init : CharacterDetailFragmentState()
        data class IsLoading(val isLoading: Boolean) : CharacterDetailFragmentState()
    }
}