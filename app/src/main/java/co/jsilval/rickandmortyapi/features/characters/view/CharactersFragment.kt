package co.jsilval.rickandmortyapi.features.characters.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.jsilval.rickandmortyapi.R
import co.jsilval.rickandmortyapi.core.common.EMPTY_RESULT
import co.jsilval.rickandmortyapi.core.common.ERROR
import co.jsilval.rickandmortyapi.core.common.ErrorType
import co.jsilval.rickandmortyapi.core.common.dialogs.ErrorDialog
import co.jsilval.rickandmortyapi.core.common.dialogs.LoadingDialog
import co.jsilval.rickandmortyapi.core.common.extension.showToast
import co.jsilval.rickandmortyapi.core.common.fragments.BaseFragment
import co.jsilval.rickandmortyapi.core.common.fragments.getDialogsManager
import co.jsilval.rickandmortyapi.core.receivers.NetworkStateCallback
import co.jsilval.rickandmortyapi.core.receivers.NetworkStateManager
import co.jsilval.rickandmortyapi.core.viewmodels.ViewModelFactory
import co.jsilval.rickandmortyapi.databinding.CharactersFragmentBinding
import co.jsilval.rickandmortyapi.features.characters.view.adapter.CharactersAdapter
import co.jsilval.rickandmortyapi.features.characters.view.adapter.entities.Character
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewmodels.AbstractCharacterViewModel
import co.jsilval.rickandmortyapi.features.characters.viewmodel.CharactersViewModel
import co.jsilval.rickandmortyapi.features.characters.viewmodel.CharactersViewModel.CharactersFragmentState
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class CharactersFragment : BaseFragment(), CharactersAdapter.CharactersAdapterListener {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by viewModels<CharactersViewModel> { viewModelFactory }
    private var _binding: CharactersFragmentBinding? = null
    private val binding get() = _binding!!
    private var snackBar: Snackbar? = null

    @Inject
    lateinit var networkStateCallback: NetworkStateCallback

    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Toast.makeText(requireContext(), "Exception: $throwable", Toast.LENGTH_SHORT).show()
    }
    private val coroutineScope =
        CoroutineScope(SupervisorJob() + Dispatchers.IO + coroutineExceptionHandler)

    private val RecyclerView.endListReached: Flow<Int>
        get() = callbackFlow {
            val lm = (layoutManager as LinearLayoutManager)
            val listener = object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0) {
                        val lastVisible = lm.findLastVisibleItemPosition()
                        val endHasBeenReached = lastVisible + 5 >= lm.itemCount
                        if (endHasBeenReached) {
                            trySend(lm.itemCount)
                        }
                    } else {
                        trySend(viewModel.invalidPage)
                    }
                }
            }
            addOnScrollListener(listener)
            awaitClose { removeOnScrollListener(listener) }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = CharactersFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerViewCharacters()
        networkStateCallback.registerNetworkCallbackEvents()
    }

    override fun onStop() {
        super.onStop()
        coroutineScope.coroutineContext.cancelChildren()
        networkStateCallback.unRegisterNetworkCallbackEvents()
    }

    override fun addObservers() {
        observeState()
        observeCharacterList()
        observeDataPagination()
        observerNetWorkState()
    }

    private fun observerNetWorkState() {
        NetworkStateManager.hasInternet.flowWithLifecycle(viewLifecycleOwner.lifecycle,
            Lifecycle.State.STARTED)
            .onEach { hasInternet ->
                handleInternetState(hasInternet)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.hasInternet
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { hasInternet ->
                handleInternetState(hasInternet)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun observeDataPagination() {
        viewModel.pagination
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { pagination ->
                viewModel.fetchCharacters(pagination)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun handleInternetState(hasInternet: Boolean) {
        when (hasInternet) {
            false -> showNoInternetAlert()
            else -> {
                snackBar?.dismiss()
            }
        }
    }

    private fun observeState() {
        viewModel.state
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { state ->
                handleState(state)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun observeCharacterList() {
        viewModel.characters
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { characters ->
                hideEmptyResultState()
                handleCharacters(characters)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    /**
     * Maneja los nuevos datos que llegan luego de consultar exitosamente el servicio /character
     */
    private fun handleCharacters(characters: List<AbstractCharacterViewModel>) {
        (binding.rvCharacters.adapter as CharactersAdapter).addItems(characters)
    }

    /**
     * Maneja los estados de la vista
     *
     * @param state inidica el nuevo estado de la vista
     */
    private fun handleState(state: CharactersFragmentState) {
        when (state) {
            is CharactersFragmentState.IsLoading -> handleLoading(state.isLoading)
            is CharactersFragmentState.ShowToast -> requireActivity().showToast(state.message)
            is CharactersFragmentState.Init -> Unit
            is CharactersFragmentState.ShowErrorInfo -> handleError(state.errorType)
        }
    }

    private fun handleLoading(isLoading: Boolean) {
        when (isLoading) {
            true -> showLoadingDialog()
            false -> hideLoadingDialog()
        }
    }

    private fun hideLoadingDialog() {
        getDialogsManager().dismissCurrentlyShownDialog()
    }

    /**
     * Muestra un diálogo de carga
     */
    private fun showLoadingDialog() {
        getDialogsManager().showDialogWithId(LoadingDialog(), "loading_dialog")
    }

    /**
     * Maneja los tipos de errores que pueden ocurrir en la vista
     */
    private fun handleError(@ErrorType errorType: Int) {
        when (errorType) {
            ERROR -> showErrorDialog()
            EMPTY_RESULT -> showEmptyResultState()
        }
    }

    private fun showNoInternetAlert() {
        snackBar = Snackbar.make(binding.rvCharacters,
            getString(R.string.text_no_internet_connection),
            Snackbar.LENGTH_INDEFINITE).let {
            it.show()
            it.setAction(R.string.text_retry) {
                viewModel.fetchCharacters()
            }
            it
        }
    }

    private fun showEmptyResultState() {
        binding.tvEmptyStateText.visibility = View.VISIBLE
        binding.ivEmptysate.visibility = View.VISIBLE
    }

    private fun hideEmptyResultState() {
        binding.tvEmptyStateText.visibility = View.GONE
        binding.ivEmptysate.visibility = View.GONE
    }

    private fun showErrorDialog() {
        getDialogsManager().showDialogWithId(ErrorDialog(), "error_dialog")
    }

    /**
     * Configuración de la lista de personajes.
     */
    private fun setupRecyclerViewCharacters() {
        val adapter = CharactersAdapter().apply {
            setListener(this@CharactersFragment)
        }
        binding.rvCharacters.adapter = adapter
        coroutineScope.launch(Dispatchers.IO) {
            binding.rvCharacters.endListReached.conflate().collectLatest { position ->
                viewModel.fetchNewPageCharacters(position)
            }
        }
    }

    override fun onClickCharacter(character: Character) {
        openCharacterDetail(character.id)
    }

    /*
    * Abre el detalle del personaje
    * */
    private fun openCharacterDetail(id: Long) {
        goTo(CharactersFragmentDirections.toCharacterDetailFragment(id),
            R.id.characterDetailFragment)
    }
}