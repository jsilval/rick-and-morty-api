package co.jsilval.rickandmortyapi.features.characters.view.adapter.viewmodels

import co.jsilval.rickandmortyapi.features.characters.view.adapter.factory.CharactersTypeFactory

abstract class AbstractCharacterViewModel(var id: Long? = null) {
    abstract fun type(typeFactory: CharactersTypeFactory): Int

    override fun equals(other: Any?): Boolean {
        if (id == null || other == null) {
            return super.equals(other)
        }

        return (other as AbstractCharacterViewModel).id == id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}