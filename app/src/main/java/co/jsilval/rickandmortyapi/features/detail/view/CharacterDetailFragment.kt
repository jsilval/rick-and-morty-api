package co.jsilval.rickandmortyapi.features.detail.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import co.jsilval.rickandmortyapi.core.common.dialogs.LoadingDialog
import co.jsilval.rickandmortyapi.core.common.fragments.BaseFragment
import co.jsilval.rickandmortyapi.core.common.fragments.getDialogsManager
import co.jsilval.rickandmortyapi.core.utils.toStatus
import co.jsilval.rickandmortyapi.core.viewmodels.ViewModelFactory
import co.jsilval.rickandmortyapi.databinding.CharacterDetailFragmentBinding
import co.jsilval.rickandmortyapi.features.detail.domain.entities.CharacterDetail
import co.jsilval.rickandmortyapi.features.detail.viewmodels.CharacterDetailViewModel
import co.jsilval.rickandmortyapi.features.detail.viewmodels.CharacterDetailViewModel.CharacterDetailFragmentState
import com.bumptech.glide.Glide
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class CharacterDetailFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by viewModels<CharacterDetailViewModel> { viewModelFactory }
    private var _binding: CharacterDetailFragmentBinding? = null
    private val binding get() = _binding!!
    private val args: CharacterDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = CharacterDetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getDetail()
        binding.btnBack.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun addObservers() {
        observeState()
        observeDetail()
    }

    private fun observeState() {
        viewModel.state
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { state ->
                handleState(state)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun observeDetail() {
        viewModel.detail
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { detail ->
                handleDetail(detail)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun handleDetail(detail: CharacterDetail?) {
        detail?.let {
            context?.let { ctx ->
                Glide.with(ctx).load(it.imageUrl).into(binding.ivPhoto)
                binding.tvName.text = it.name
                binding.tvStatus.text = String.format("%s -", it.status)
                binding.tvSpecies.text = it.species
                val drawableRes = toStatus(it.status).drawableRes
                binding.tvStatus.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(
                    ctx, drawableRes), null, null, null)
            }
        }
    }

    private fun handleState(state: CharacterDetailFragmentState) {
        when (state) {
            is CharacterDetailFragmentState.IsLoading -> handleLoading(state.isLoading)
            is CharacterDetailFragmentState.Init -> Unit
        }
    }

    private fun handleLoading(isLoading: Boolean) {
        when (isLoading) {
            true -> showLoadingDialog()
            false -> hideLoadingDialog()
        }
    }

    private fun hideLoadingDialog() {
        getDialogsManager().dismissCurrentlyShownDialog()
    }

    /**
     * Muestra un diálogo de carga
     */
    private fun showLoadingDialog() {
        getDialogsManager().showDialogWithId(LoadingDialog(), "loading_dialog")
    }

    private fun getDetail() {
        viewModel.getCharacterDetail(args.characterId)
    }
}