package co.jsilval.rickandmortyapi.features.characters.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.jsilval.rickandmortyapi.core.Result
import co.jsilval.rickandmortyapi.core.common.ErrorType
import co.jsilval.rickandmortyapi.core.common.NO_INTERNET
import co.jsilval.rickandmortyapi.core.log.AppLog
import co.jsilval.rickandmortyapi.features.characters.domain.usecase.GetCharacters
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewmodels.AbstractCharacterViewModel
import co.jsilval.rickandmortyapi.features.characters.viewmodel.CharactersViewModel.CharactersFragmentState.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

class CharactersViewModel @Inject constructor(private val getCharacters: GetCharacters) :
    ViewModel() {

    val invalidPage = -1
    private val _state = MutableStateFlow<CharactersFragmentState>(Init)
    val state: SharedFlow<CharactersFragmentState> get() = _state

    private val _characters = MutableStateFlow<List<AbstractCharacterViewModel>>(mutableListOf())
    val characters: SharedFlow<List<AbstractCharacterViewModel>> get() = _characters

    private val _pagination = MutableStateFlow(0)
    val pagination: SharedFlow<Int> get() = _pagination

    private val _hasInternet = MutableStateFlow(true)
    val hasInternet: SharedFlow<Boolean> get() = _hasInternet

    init {
        _pagination.value = 0
    }

    private fun showErrorInfo(errorType: Int) {
        _state.value = ShowErrorInfo(errorType)
    }

    private fun showLoading() {
        _state.value = IsLoading(true)
    }

    private fun hideLoading() {
        _state.value = IsLoading(false)
    }

    private fun showToast(message: String) {
        _state.value = ShowToast(message)
    }

    /**
     * Trae la lista de personajes
     * La vista pasa desde el estado "Cargando" hasta el estado de éxito o error.
     */
    fun fetchCharacters(pagination: Int = 0) {
        _hasInternet.value = true
        if (pagination == invalidPage) {
            return
        }
        viewModelScope.launch {
            if (pagination == 0) {
                showLoading()
            }
            getCharacters.getCharacters()
                .catch { exception ->
                    hideLoading()
                    showToast(exception.message.toString())
                    AppLog.e("Error Obteniendo lista de personajes.", exception)
                }.collect { result ->
                    when (result) {
                        is Result.Error -> {
                            hideLoading()
                            when (result.data) {
                                NO_INTERNET -> _hasInternet.value = false
                                else -> showErrorInfo(result.data)
                            }
                        }
                        is Result.Success -> {
                            _characters.value = result.data
                            hideLoading()
                        }
                    }
                }
        }
    }

    fun fetchNewPageCharacters(position: Int) {
        _pagination.value = position
    }

    /**
     * Clase para representar estados posibles de la vista CharactersFragment
     * @property Init representa el estado inicial de la vista.
     * @property IsLoading indica si la vista está mostrando un diálogo de espera
     * @property ShowToast inidica que la vista debe mostrar un mensaje en un toast.
     * @property ShowErrorInfo inidica que ha ocurrido un error y se muestra un diálogo.
     */
    sealed class CharactersFragmentState {
        object Init : CharactersFragmentState()
        data class ShowToast(val message: String) : CharactersFragmentState()
        data class IsLoading(val isLoading: Boolean) : CharactersFragmentState()
        data class ShowErrorInfo(@ErrorType val errorType: Int) : CharactersFragmentState()
    }
}