package co.jsilval.rickandmortyapi.features.characters.view.adapter.viewholder

import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import co.jsilval.rickandmortyapi.R
import co.jsilval.rickandmortyapi.core.utils.toStatus
import co.jsilval.rickandmortyapi.databinding.CharacterItemLayoutBinding
import co.jsilval.rickandmortyapi.features.characters.view.adapter.entities.Character
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewmodels.CharacterViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions


class CharacterViewHolder(private val binding: CharacterItemLayoutBinding) :
    AbstractViewHolder<CharacterViewModel>(binding),
    View.OnClickListener {

    companion object {
        const val LAYOUT: Int = R.layout.character_item_layout
    }

    private lateinit var character: Character

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        listener?.onCharacterItemClicked(character)
    }

    override fun bind(viewModel: CharacterViewModel) {
        character = viewModel.character
        Glide.with(itemView.context)
            .load(character.imageUrl)
            .apply(RequestOptions.bitmapTransform(CircleCrop()))
            .into(binding.ivImage)

        binding.tvCharacterName.text = character.name
        binding.tvStatus.text = character.status
        setStatusDrawable(character.status)
    }

    private fun setStatusDrawable(status: String) {
        val drawableRes = toStatus(status).drawableRes
        binding.tvStatus.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(
            itemView.context, drawableRes), null, null, null)
    }
}
