package co.jsilval.rickandmortyapi.features.characters.view.adapter

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.jsilval.rickandmortyapi.features.characters.view.adapter.entities.Character
import co.jsilval.rickandmortyapi.features.characters.view.adapter.factory.CharactersTypeFactoryForList
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewholder.AbstractViewHolder
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewmodels.AbstractCharacterViewModel

class CharactersAdapter :
    RecyclerView.Adapter<AbstractViewHolder<AbstractCharacterViewModel>>(),
     AbstractViewHolder.OnItemClickedListener {

    private var listener: CharactersAdapterListener? = null
    private lateinit var context: Context
    private var typeFactory = CharactersTypeFactoryForList()

    private var items: List<AbstractCharacterViewModel> = ArrayList()

    override fun onBindViewHolder(holder: AbstractViewHolder<AbstractCharacterViewModel>, position: Int) {
        val item = items[position]
        holder.bind(item)
        holder.setOnItemClickListener(this)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder<AbstractCharacterViewModel> {
        context = parent.context
        val binding = typeFactory.createViewBinding(parent, viewType)
        return typeFactory.createViewHolder(binding, viewType) as AbstractViewHolder<AbstractCharacterViewModel>
    }

    override fun getItemViewType(position: Int): Int = items[position].type(typeFactory)

    override fun getItemCount() = items.count()

    fun addItems(items: List<AbstractCharacterViewModel>) {
        val oldSize = this.items.size
        (this.items as ArrayList).addAll(items)
        notifyItemChanged(oldSize, items.size)
    }

    override fun onCharacterItemClicked(character: Character) {
        listener?.onClickCharacter(character)
    }

    fun setListener(listener: CharactersAdapterListener) {
        this.listener = listener
    }

    interface CharactersAdapterListener {
        fun onClickCharacter(character: Character)
    }
}

