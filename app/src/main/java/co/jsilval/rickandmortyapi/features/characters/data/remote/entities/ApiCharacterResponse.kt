package co.jsilval.rickandmortyapi.features.characters.data.remote.entities

import com.google.gson.annotations.SerializedName

data class ApiCharacterResponse(

	@field:SerializedName("results")
	val results: List<ResultsItem>? = null,

	@field:SerializedName("info")
	val info: Info,
)

data class ResultsItem(

	@field:SerializedName("image")
	val image: String,

	@field:SerializedName("gender")
	val gender: String,

	@field:SerializedName("species")
	val species: String,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("origin")
	val origin: Origin? = null,

	@field:SerializedName("name")
	val name: String,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("episode")
	val episode: List<String?>? = null,

	@field:SerializedName("id")
	val id: Long,

	@field:SerializedName("type")
	val type: String,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("status")
	val status: String
)

data class Location(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)

data class Info(

	@field:SerializedName("next")
	val next: String? = null,

	@field:SerializedName("pages")
	val pages: Int? = null,

	@field:SerializedName("prev")
	val prev: Any? = null,

	@field:SerializedName("count")
	val count: Int? = null
)

data class Origin(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)
