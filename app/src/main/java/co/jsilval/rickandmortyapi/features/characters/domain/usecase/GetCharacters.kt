package co.jsilval.rickandmortyapi.features.characters.domain.usecase

import android.content.Context
import co.jsilval.rickandmortyapi.core.Result
import co.jsilval.rickandmortyapi.core.common.NO_INTERNET
import co.jsilval.rickandmortyapi.core.receivers.NetworkStateManager
import co.jsilval.rickandmortyapi.features.characters.data.CharactersRepositoryImp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GetCharacters @Inject constructor(
    private val context: Context,
    private val repository: CharactersRepositoryImp,
) {

    suspend fun getCharacters() = flow {
        val hasInternet = NetworkStateManager.isInternetConnected(context)
        if (!hasInternet) {
            emit(Result.Error(NO_INTERNET))
        }
        repository.getCharacters(hasInternet).collect {
            emit(it)
        }
    }.flowOn(Dispatchers.IO)

    suspend fun getDetailCharacter(id: Long) = repository.getCharacterDetail(id)
}