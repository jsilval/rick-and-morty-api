package co.jsilval.rickandmortyapi.features.characters.view.adapter.entities

data class Character(
    val id: Long,
    val name: String,
    val status: String,
    val imageUrl: String,
)