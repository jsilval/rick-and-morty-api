package co.jsilval.rickandmortyapi.features.characters.view.adapter.viewholder

import androidx.viewbinding.ViewBinding
import androidx.recyclerview.widget.RecyclerView
import co.jsilval.rickandmortyapi.features.characters.view.adapter.entities.Character

abstract class AbstractViewHolder<in T>(binding: ViewBinding) : RecyclerView.ViewHolder(binding.root) {

    var listener: OnItemClickedListener? = null

    abstract fun bind(viewModel: T)

    fun setOnItemClickListener(listener: OnItemClickedListener) {
        this.listener = listener
    }

    interface OnItemClickedListener {
        fun onCharacterItemClicked(character: Character)
    }
}
