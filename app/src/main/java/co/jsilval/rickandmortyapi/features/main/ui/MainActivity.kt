package co.jsilval.rickandmortyapi.features.main.ui

import android.os.Bundle
import co.jsilval.rickandmortyapi.core.common.activities.BaseActivity
import co.jsilval.rickandmortyapi.databinding.ActivityMainBinding

class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}