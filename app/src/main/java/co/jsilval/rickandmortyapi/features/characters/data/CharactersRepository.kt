package co.jsilval.rickandmortyapi.features.characters.data

import co.jsilval.rickandmortyapi.core.Result
import co.jsilval.rickandmortyapi.core.common.ErrorType
import co.jsilval.rickandmortyapi.features.characters.view.adapter.viewmodels.AbstractCharacterViewModel
import co.jsilval.rickandmortyapi.features.detail.domain.entities.CharacterDetail
import kotlinx.coroutines.flow.Flow

interface CharactersRepository {
    suspend fun getCharacters(hasInternet: Boolean): Flow<Result<List<AbstractCharacterViewModel>, @ErrorType Int>>
    suspend fun getCharacterDetail(id: Long): Flow<CharacterDetail>
}