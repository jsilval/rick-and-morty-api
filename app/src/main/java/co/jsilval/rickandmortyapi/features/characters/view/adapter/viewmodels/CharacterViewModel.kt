package co.jsilval.rickandmortyapi.features.characters.view.adapter.viewmodels

import co.jsilval.rickandmortyapi.features.characters.view.adapter.entities.Character
import co.jsilval.rickandmortyapi.features.characters.view.adapter.factory.CharactersTypeFactory

class CharacterViewModel(var character: Character) : AbstractCharacterViewModel() {

    init {
        id = character.id
    }

    override fun type(typeFactory: CharactersTypeFactory): Int {
        return typeFactory.type(character)
    }
}
