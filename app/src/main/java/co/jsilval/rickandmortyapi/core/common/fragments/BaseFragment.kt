package co.jsilval.rickandmortyapi.core.common.fragments

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import co.jsilval.rickandmortyapi.core.di.Injectable

abstract class BaseFragment : Fragment(), Injectable {

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        addObservers()
    }

    open fun addObservers() {}

    open fun goTo(direction: NavDirections, destinationId: Int) {
        val navController = findNavController()

        navController.popBackStack(destinationId, false).let { wasPopped ->
            if (!wasPopped) {
                navController.navigate(direction)
            }
        }
    }
}