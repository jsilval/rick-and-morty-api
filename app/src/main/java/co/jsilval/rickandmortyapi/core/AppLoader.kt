package co.jsilval.rickandmortyapi.core

import android.content.Context
import androidx.multidex.MultiDexApplication
import co.jsilval.apimanager.core.rest.network.ApiManager
import co.jsilval.rickandmortyapi.core.di.AppInjector
import co.jsilval.rickandmortyapi.core.common.LANDING_SERVER
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import java.lang.ref.WeakReference
import javax.inject.Inject

class AppLoader : MultiDexApplication(), HasAndroidInjector {

    companion object {
        var weakReference: WeakReference<Context>? = null
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        setApiValues()
        AppInjector.init(this)
        weakReference = WeakReference(applicationContext)
    }

    /**
     * Valores para el consumo de los servicios REST
     */
    private fun setApiValues() {
        ApiManager.getInstance().connectTimeout(30000)
        ApiManager.getInstance().readTimeout(30000)
        ApiManager.getInstance().baseURL = LANDING_SERVER
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector
}