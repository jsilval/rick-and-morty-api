package co.jsilval.rickandmortyapi.core.db.character

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface CharacterDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(characters: List<CharacterDB>)

    @Query("Select * From CharacterDB Where `id`= :id")
    fun getCharacterById(id: Long): CharacterDB

    @Query("Select * From CharacterDB")
    fun getCharacters(): Flow<List<CharacterDB>>

    @Query("DELETE FROM CharacterDB")
    fun clearTable()
}