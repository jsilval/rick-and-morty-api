package co.jsilval.rickandmortyapi.core.utils

import co.jsilval.rickandmortyapi.R

sealed class Status(val drawableRes: Int) {
    object ALIVE : Status(R.drawable.alive_state)
    object UNKNOWN : Status(R.drawable.unknown_state)
    object DEAD : Status(R.drawable.dead_state)
}

fun toStatus(status: String): Status {
    return when (status.lowercase()) {
        "alive" -> Status.ALIVE
        "dead" -> Status.DEAD
        else -> Status.UNKNOWN
    }
}