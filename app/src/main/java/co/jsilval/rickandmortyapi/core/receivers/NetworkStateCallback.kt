package co.jsilval.rickandmortyapi.core.receivers

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import javax.inject.Inject


class NetworkStateCallback @Inject constructor(val context: Context) :
    ConnectivityManager.NetworkCallback() {

    private var connectivityManager: ConnectivityManager? = null

    private var networkRequest: NetworkRequest = NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .build()

    init {
        connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    override fun onAvailable(network: Network) {
        super.onAvailable(network)
        NetworkStateManager.setNetworkConnectivityStatus(true)
    }

    override fun onLost(network: Network) {
        super.onLost(network)
        NetworkStateManager.setNetworkConnectivityStatus(false)
    }

    fun registerNetworkCallbackEvents() {
        connectivityManager?.registerNetworkCallback(networkRequest, this)
    }

    fun unRegisterNetworkCallbackEvents() {
        connectivityManager?.unregisterNetworkCallback(this)
    }
}