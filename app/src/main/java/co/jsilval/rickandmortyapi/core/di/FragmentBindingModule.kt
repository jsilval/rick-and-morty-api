package co.jsilval.rickandmortyapi.core.di

import co.jsilval.rickandmortyapi.features.characters.view.CharactersFragment
import co.jsilval.rickandmortyapi.features.detail.view.CharacterDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeProductsFragment(): CharactersFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeProductDetailFragment(): CharacterDetailFragment
}