package co.jsilval.rickandmortyapi.core.common.dialogs

import android.os.Bundle
import android.text.TextUtils
import androidx.annotation.Nullable
import androidx.annotation.UiThread
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import javax.inject.Inject

@UiThread
class DialogsManager @Inject constructor(private val fragmentManager: FragmentManager) {

    companion object {
        /**
         * Cuando un diálogo es mostrado con un id que no sea vacío, éste se almacena como argumento
         * dentro de un Bundle con este key.
         */
        const val ARGUMENT_DIALOG_ID = "ARGUMENT_DIALOG_ID"

        /**
         * Tag para referenciar el diálogo que se está mostrando, puede ser usado para buscar el diálogo con
         * la clase FragmentManager
         */
        private const val DIALOG_FRAGMENT_TAG = "DIALOG_FRAGMENT_TAG"
    }

    private var mCurrentlyShownDialog: DialogFragment? = null

    init {
        val fragmentWithDialogTag: Fragment? =
            fragmentManager.findFragmentByTag(DIALOG_FRAGMENT_TAG)

        // there might be some dialog already shown
        if (fragmentWithDialogTag != null &&
            DialogFragment::class.java.isAssignableFrom(fragmentWithDialogTag.javaClass)
        ) {
            mCurrentlyShownDialog = fragmentWithDialogTag as DialogFragment
        }
    }

    /**
     * @return una referencia al diálogo actual que es mostrado, sería null si no se está mostrando
     * algún diálogo.
     */
    @Nullable
    fun getCurrentlyShownDialog(): DialogFragment? {
        return mCurrentlyShownDialog
    }

    /**
     * Obtiene el id del diálog que se muestra en ese momento.
     *
     * @return el id del diálogo que se muestra actualmente; devuelve un string vacío si no se
     * está mostrando algún diálogo o el diálogo que se muestra no tiene id.
     */
    fun getCurrentlyShownDialogId(): String? {
        return if (mCurrentlyShownDialog == null || mCurrentlyShownDialog!!.arguments == null ||
            !mCurrentlyShownDialog!!.requireArguments().containsKey(ARGUMENT_DIALOG_ID)
        ) {
            ""
        } else {
            mCurrentlyShownDialog!!.requireArguments().getString(ARGUMENT_DIALOG_ID)
        }
    }

    /**
     * Revisa si un diálogo con un id específico se está mostrando.
     *
     * @param id id del diálogo para verificar.
     * @return true si el diálogo con el id dado se está mostrando, falso en el caso contrario.
     */
    fun isDialogCurrentlyShown(id: String): Boolean {
        val shownDialogId = getCurrentlyShownDialogId()
        return !TextUtils.isEmpty(shownDialogId) && shownDialogId == id
    }

    /**
     * Oculta el diálogo que se está mostrando.
     */
    fun dismissCurrentlyShownDialog() {
        if (mCurrentlyShownDialog != null) {
            mCurrentlyShownDialog!!.dismissAllowingStateLoss()
            mCurrentlyShownDialog = null
        }
    }

    /**
     * Muestra un diálogo y le asigna un "id". Reemplaza cualquier otro diálogo que se esté mostrando.
     *
     * @param dialog dialog par mostrar.
     * @param id identificador único para el diálogo.
     */
    fun showDialogWithId(dialog: DialogFragment, @Nullable id: String?) {
        dismissCurrentlyShownDialog()
        setId(dialog, id)
        showDialog(dialog)
    }

    private fun setId(dialog: DialogFragment, @Nullable id: String?) {
        val args = if (dialog.arguments != null) dialog.arguments else Bundle(1)
        args!!.putString(ARGUMENT_DIALOG_ID, id)
        dialog.arguments = args
    }

    private fun showDialog(dialog: DialogFragment) {
        fragmentManager.beginTransaction()
            .add(dialog, DIALOG_FRAGMENT_TAG)
            .commitAllowingStateLoss()
        mCurrentlyShownDialog = dialog
    }
}