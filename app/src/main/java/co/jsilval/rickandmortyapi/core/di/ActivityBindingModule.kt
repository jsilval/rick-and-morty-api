package co.jsilval.rickandmortyapi.core.di

import co.jsilval.rickandmortyapi.features.main.ui.MainActivity
import co.jsilval.rickandmortyapi.features.main.di.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentBindingModule::class, MainActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity
}
