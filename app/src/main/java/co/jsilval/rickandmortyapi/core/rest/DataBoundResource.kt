package co.jsilval.rickandmortyapi.core.rest

import co.jsilval.rickandmortyapi.core.Result
import co.jsilval.rickandmortyapi.core.common.ErrorType
import kotlinx.coroutines.flow.Flow

abstract class DataBoundResource<ResultType : Any, RequestType> {

    private suspend fun fetchFromNetwork() {
        val response = executeCallService()
        if (response != null) {
            if (isNotEmptyResult(response)) {
                saveCallServiceResult(response)
            }
        }
    }

    private suspend fun internalLoadFromDB(): Flow<Result<ResultType, @ErrorType Int>> {
        if (shouldFetch()) {
            fetchFromNetwork()
        }
        return loadFromDb()
    }

    suspend fun asFlow() = internalLoadFromDB()

    protected abstract fun shouldFetch(): Boolean

    protected abstract suspend fun saveCallServiceResult(response: RequestType)

    protected abstract suspend fun executeCallService(): RequestType?

    abstract fun isNotEmptyResult(result: RequestType): Boolean

    protected abstract fun loadFromDb(): Flow<Result<ResultType, @ErrorType Int>>
}