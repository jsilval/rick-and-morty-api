package co.jsilval.rickandmortyapi.core.common.views

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.TextView
import android.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import co.jsilval.rickandmortyapi.R

class AppToolbar : Toolbar {
    private var tvTitle: TextView? = null

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)

        val a = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.AppToolbar,
            0, 0
        )
        val toolbarTitle = a.getString(R.styleable.AppToolbar_title) ?: "Title"
        val color = a.getInteger(R.styleable.AppToolbar_titleColor, 0xff0000)

        title = toolbarTitle
        tvTitle?.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24f)
        setTitleTextColor(color)
        if (!isInEditMode) {
            tvTitle?.typeface = ResourcesCompat.getFont(context, R.font.roboto_bold)
        }
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        tvTitle = TextView(context).apply {
            setSingleLine()
            ellipsize = TextUtils.TruncateAt.END
            gravity = Gravity.CENTER
//            setTextAppearance(context,
//               androidx.appcompat.R.style.TextAppearance_AppCompat_Widget_ActionBar_Title)
            val lp = LayoutParams(androidx.appcompat.widget.Toolbar.LayoutParams.WRAP_CONTENT,
                androidx.appcompat.widget.Toolbar.LayoutParams.WRAP_CONTENT).apply {
                gravity = Gravity.CENTER
            }
            layoutParams = lp
        }
        addView(tvTitle)
    }

    override fun setTitle(resId: Int) {
        val s = resources.getString(resId)
        title = s
    }

    override fun setTitle(title: CharSequence) {
        tvTitle?.text = title
    }

    override fun getTitle(): CharSequence {
        return tvTitle?.text ?: ""
    }

    override fun setTitleTextColor(color: Int) {
        super.setTitleTextColor(color)
        tvTitle?.setTextColor(color)
    }
}