package co.jsilval.rickandmortyapi.core.receivers

import android.content.Context
import android.net.ConnectivityManager
import co.jsilval.rickandmortyapi.core.common.REACHABLE_SERVER
import co.jsilval.rickandmortyapi.core.log.AppLog
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL


object NetworkStateManager {
    private val _hasInternet = MutableStateFlow(true)
    val hasInternet: SharedFlow<Boolean> get() = _hasInternet

    fun setNetworkConnectivityStatus(hasInternet: Boolean) {
        _hasInternet.value = hasInternet
    }

    /**
     * Verifica si la red está activa.
     */
    private fun isNetworkAvailable(context: Context): Boolean {
        val service = Context.CONNECTIVITY_SERVICE
        val manager = context.getSystemService(service) as ConnectivityManager?
        val network = manager?.activeNetworkInfo
        AppLog.d("hasNetworkAvailable: ${(network != null)}")
        return (network != null)
    }

    /**
     * Comprueba que la res esté activa y si es el caso realiza una petición a google
     * para comprobar si hay conexión a internet.
     */
    fun isInternetConnected(context: Context): Boolean {
        if (isNetworkAvailable(context)) {
            try {
                val connection = URL(REACHABLE_SERVER).openConnection() as HttpURLConnection
                connection.setRequestProperty("User-Agent", "ConnectionTest")
                connection.setRequestProperty("Connection", "close")
                connection.connectTimeout = 1000
                connection.connect()
                AppLog.d("hasInternetConnected: ${(connection.responseCode == 200)}")
                return (connection.responseCode == 200)
            } catch (e: IOException) {
                AppLog.e("Error checking internet connection", e)
            }
        } else {
            AppLog.w("No network available!")
        }
        AppLog.d("hasInternetConnected: false")
        return false
    }
}