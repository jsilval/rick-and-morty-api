package co.jsilval.rickandmortyapi.core.db.character

import androidx.room.Entity
import co.jsilval.rickandmortyapi.features.characters.view.adapter.entities.Character
import co.jsilval.rickandmortyapi.features.detail.domain.entities.CharacterDetail

@Entity(primaryKeys = ["id"])
data class CharacterDB(
    val id: Long,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val imageUrl: String,
) {
    fun toCharacterDetailModel() =
        CharacterDetail(id, name, status, species, type, gender, imageUrl)

    fun toCharacterModel() = Character(id, name, status, imageUrl)
}