package co.jsilval.rickandmortyapi.core.common

import androidx.annotation.IntDef

const val LANDING_SERVER = "https://rickandmortyapi.com/"
const val REACHABLE_SERVER = "https://www.google.com/"

/**
 * Tipos de error:
 *
 * @property ERROR error general.
 * @property EMPTY_RESULT error que indica que no se obtuvieron datos.
 * @property NO_INTERNET error que indica que no hay conexión a internet.
 */
@Target(AnnotationTarget.PROPERTY, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.TYPE)
@IntDef(ERROR, EMPTY_RESULT, NO_INTERNET)
@Retention(AnnotationRetention.SOURCE)
annotation class ErrorType

const val ERROR = 0
const val EMPTY_RESULT = 1
const val NO_INTERNET = 2

