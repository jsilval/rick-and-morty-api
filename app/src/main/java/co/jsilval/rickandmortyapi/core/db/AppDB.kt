package co.jsilval.rickandmortyapi.core.db

import androidx.room.Database
import androidx.room.RoomDatabase
import co.jsilval.rickandmortyapi.core.db.character.CharacterDB
import co.jsilval.rickandmortyapi.core.db.character.CharacterDao

@Database(
    entities = [
        CharacterDB::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDB : RoomDatabase() {

    abstract fun charactersDao(): CharacterDao
}