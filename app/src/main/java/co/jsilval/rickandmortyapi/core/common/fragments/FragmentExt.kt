package co.jsilval.rickandmortyapi.core.common.fragments

import co.jsilval.rickandmortyapi.core.common.activities.BaseActivity
import co.jsilval.rickandmortyapi.core.common.dialogs.DialogsManager

fun BaseFragment.getDialogsManager(): DialogsManager {
    return (activity as BaseActivity).dialogsManager
}