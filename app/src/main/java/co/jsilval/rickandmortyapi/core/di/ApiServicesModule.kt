package co.jsilval.rickandmortyapi.core.di

import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.headers.Header
import co.jsilval.apimanager.core.rest.network.ApiClient
import co.jsilval.apimanager.core.rest.network.HttpMethod
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiServicesModule {

    @Singleton
    @Named("character")
    @Provides
    fun provideCharacterClient(@Named("character") request: ServerRequest): ApiClient {
        return ApiClient(request, HttpMethod.GET)
    }

    @Singleton
    @Named("character")
    @Provides
    fun provideSearchRequest(): ServerRequest {
        return ServerRequest.create("/api/character")
            .header(Header.CONTENT_TYPE, "application/json")
    }
}
