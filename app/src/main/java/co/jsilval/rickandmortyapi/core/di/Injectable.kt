package co.jsilval.rickandmortyapi.core.di

/**
 * Marca una activity / fragment como injectable.
 */
interface Injectable
