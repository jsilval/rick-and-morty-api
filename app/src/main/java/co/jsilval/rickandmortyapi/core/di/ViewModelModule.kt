package co.jsilval.rickandmortyapi.core.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.jsilval.rickandmortyapi.core.viewmodels.ViewModelFactory
import co.jsilval.rickandmortyapi.features.characters.viewmodel.CharactersViewModel
import co.jsilval.rickandmortyapi.features.detail.viewmodels.CharacterDetailViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CharactersViewModel::class)
    abstract fun bindProductsViewModel(viewModel: CharactersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CharacterDetailViewModel::class)
    abstract fun bindProductDetailViewModel(viewModel: CharacterDetailViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
